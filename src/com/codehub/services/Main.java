package com.codehub.services;

import java.util.List;

import com.codehub.model.Item;
import com.codehub.services.StaxParser;

public class Main {
	
	public static void main(String [] args) throws Exception {
		
		List<Item> list = StaxParser.readConfig("test.xml");
		System.out.println(list);
		//StaxParser.saveConfig(list, "testSave.xml");
		
		JsonIOImpl jsonIOImpl = new JsonIOImpl();
		jsonIOImpl.saveToFileJSON(list, "items.json");
		
	}

}
