package com.codehub.services;

import java.util.List;

import com.codehub.model.Item;

public interface JsonIO {
	
	void saveToFileJSON(List<Item> items, String filename);
	
	List<Item> readJSON(String filename);

}
