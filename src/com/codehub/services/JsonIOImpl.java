package com.codehub.services;

import java.io.FileWriter;
import java.io.IOException;
import java.util.List;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

import com.codehub.model.Item;

public class JsonIOImpl implements JsonIO {

	@Override
	public void saveToFileJSON(List<Item> items, String filename) {
		JSONObject obj = new JSONObject();
		obj.put("Name", "crunchify.com");
		obj.put("Author", "App Shah");
 
		JSONArray company = new JSONArray();
		company.add("Compnay: eBay");
		company.add("Compnay: Paypal");
		company.add("Compnay: Google");
		obj.put("Company List", company);
		
		
 
		// try-with-resources statement based on post comment below :)
		try (FileWriter file = new FileWriter(filename)) {
			file.write(obj.toJSONString());
			System.out.println("Successfully Copied JSON Object to File...");
			System.out.println("\nJSON Object: " + obj);
		}
		catch(IOException e){
			e.printStackTrace();
			
		}
		
	}

	@Override
	public List<Item> readJSON(String filename) {
		
		return null;
	}
	
	

}
